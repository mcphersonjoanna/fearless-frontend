

  window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/states/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
        const alertMessage = `Error: ${response.status} ${response.statusText}`;
        const alert = sendAlert(alertMessage);

        // Add the alert to your HTML
        const error = document.querySelector('#error-message');
        error.innerHTML = alert;
      } else {
        const data = await response.json();
        const selectTag = document.querySelector('#state');

        for (let state of data.states) {
          const option = document.createElement('option')
          option.value  = state.abbreviation;
          option.innerHTML = state.name;
          selectTag.appendChild(option)
        }

      }
    } catch (e) {
      console.error(e);
    }
    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event => {
      event.preventDefault();
      const formData = new FormData(formTag);
      const json = JSON.stringify(Object.fromEntries(formData));

      const locationUrl = 'http://localhost:8000/api/locations/';
      const fetchConfig = {
        method: "POST",
        body: json,
        headers: {
          'Content-Type': 'application/json',
        },
      };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
          formTag.reset();
          const newLocation = await response.json();
        }
      })
    });
