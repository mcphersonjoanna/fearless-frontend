

  window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
        const alertMessage = `Error: ${response.status} ${response.statusText}`;
        const alert = sendAlert(alertMessage);

        // Add the alert to your HTML
        const error = document.querySelector('#error-message');
        error.innerHTML = alert;
      } else {
        const data = await response.json();
        const selectTag = document.getElementById('conference');

        for (let conference of data.conferences) {
          const option = document.createElement('option');
          option.value = conference.id;
          console.log("optionvalue",option.value)
          option.innerHTML = conference.name;
          selectTag.appendChild(option);
        }


      }
    } catch (e) {
      console.error(e);
    }
    const formTag = document.getElementById('create-presentation-form');
    formTag.addEventListener('submit', async event => {
      event.preventDefault();
      const formData = new FormData(formTag);
      const json = JSON.stringify(Object.fromEntries(formData));
      const confSelectTag = document.getElementById('conference');
      console.log("formdata",formData)
      console.log("Selecttag",confSelectTag)

      const conferenceId = confSelectTag.value
      const presentationUrl = `http://localhost:8000/api/conferences/${conferenceId}/presentations/`;
      const fetchConfig = {
        method: "POST",
        body: json,
        headers: {
          'Content-Type': 'application/json',
        },
      };
        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
          formTag.reset();
          const newpresentation = await response.json();
        }
      })
    });
