function createCard(name, description, pictureUrl,start,end,location) {
  return `
  <div class="grid gap-0 row-gap-3">
  <div class="shadow p-3 mb-5 bg-body-tertiary rounded">
    <div class="card">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer">
      ${start}
      -
      ${end}
      </div>
    </div>
    </div>
    </div>
  `;
}

function sendAlert(message) {
  return `<div class="alert alert-danger" role="alert">
 ${message}
</div>`;
}

window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      // Figure out what to do when the response is bad
      const alertMessage = `Error: ${response.status} ${response.statusText}`;
      const alert = sendAlert(alertMessage);

      // Add the alert to your HTML
      const error = document.querySelector('#error-message');
      error.innerHTML = alert;

    }
    else {
      const data = await response.json();


      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          // // placeholder
          // const placeholder = createPlaceholder()
          // const holderColumn = document.querySelector('#placeholder');
          // holderColumn.innerHTML = placeholder

          // not placeholder
          const details = await detailResponse.json();
          console.log(details)
          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const start = details.conference.starts
          const end = details.conference.ends
          const startFormatted =  (new Date(start)).toLocaleDateString()
          const endFormatted = (new Date(end)).toLocaleDateString()
          const location = details.conference.location.name
          const html = createCard(title, description, pictureUrl, startFormatted, endFormatted, location );
          const column1 = document.querySelector('#column1');
          const column2 = document.querySelector('#column2');
          if (column1.childElementCount <= column2.childElementCount) {
            column1.innerHTML += html;
          } else {
            column2.innerHTML += html;
          }

        }
        else {
            // response is bad
            const alertMessage = `Error: ${response.status} ${response.statusText}`;
            const alert = sendAlert(alertMessage);

            // Add the alert to your HTML
            const error = document.querySelector('#error-message');
            error.innerHTML = alert;
        }
      }

    }
  } catch (e) {
    console.error(e);
  }

});
