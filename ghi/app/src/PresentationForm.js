import React, {useEffect, useState } from 'react';

function PresentationForm() {

  const [conference, setConference] = useState('');
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [company_name, setCompanyName] = useState('');
  const [title, setTitle] = useState('');
  const [synopsis, setSynopsis] = useState('');
  const [conferences, setConferences] = useState([]);

  const fetchData = async () => {
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setConferences(data.conferences);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.presenter_name = name;
    data.presenter_email = email;
    data.company_name= company_name;
    data.title = title;
    data.synopsis = synopsis;

    const presentationUrl = `http://localhost:8000${conference}presentations/`;
    const fetchOptions = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const presentationResponse = await fetch(presentationUrl, fetchOptions);
    if (presentationResponse.ok) {
      setConference('');
      setName('');
      setEmail('');
      setTitle("");
      setSynopsis("");
      setCompanyName("");
    }
  }

  const handleChangeConference = (event) => {
    const value = event.target.value;
    setConference(value);
  }

  const handleChangeName = (event) => {
    const value = event.target.value;
    setName(value);
  }
  const handleChangeCompanyName = (event) => {
    const value = event.target.value;
    setCompanyName(value);
  }

  const handleChangeEmail = (event) => {
    const value = event.target.value;
    setEmail(value);
  }


  const handleChangeTitle = (event) => {
    const value = event.target.value;
    setTitle(value);
  }
  const handleChangeSynopsis = (event) => {
    const value = event.target.value;
    setSynopsis(value);
  }
  return (
    <div className="row">
    <div className="offset-3 col-6">
      <div className="shadow p-4 mt-4">
        <h1>Create a new presentation</h1>
        <form onSubmit={handleSubmit} id="create-presentation-form">
          <div className="form-floating mb-3">
            <input placeholder="Name" onChange={handleChangeName} value={name} name="presenter_name" required type="text" id="presenter_name" className="form-control"/>
            <label htmlFor="name">Presenter Name</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handleChangeEmail} placeholder="Presenter Email" value={email} name="presenter_email" required type="email" id="presenter_email" className="form-control"/>
            <label htmlFor="presenter_email">Presenter Email </label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handleChangeCompanyName} placeholder="Company Name" value={company_name} name="company_name" required type="text" id="company_name" className="form-control"/>
            <label htmlFor="company_name">Company Name</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handleChangeTitle} placeholder="Title" value={title} name="title" required type="text" id="title" className="form-control"/>
            <label htmlFor="Title">Title</label>
          </div>
          <div className="mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea onChange={handleChangeSynopsis} value={synopsis} className="form-control" id="synopsis" rows="3" name="synopsis" ></textarea>
              </div>
          <div className="mb-3">
            <select onChange={handleChangeConference} value={conference} required id="conference" name="conference" className="form-select">
              <option value="">Choose a conference</option>
              {conferences.map(conference => {
                      return (
                        <option key={conference.href} value={conference.href}>{conference.name}</option>
                      )
                    })}
            </select>
          </div>
          <button className="btn btn-primary">Create</button>
        </form>
      </div>
    </div>
  </div>
  );
}

export default PresentationForm;
